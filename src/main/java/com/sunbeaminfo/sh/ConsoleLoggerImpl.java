package com.sunbeaminfo.sh;

import org.springframework.stereotype.Component;

@Component
public class ConsoleLoggerImpl implements Logger {
	@Override
	public void log(String message) {
		System.out.println("Console: " + message);
	}
}
