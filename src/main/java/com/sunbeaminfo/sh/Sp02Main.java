package com.sunbeaminfo.sh;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Sp02Main {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx;
		ctx = new ClassPathXmlApplicationContext("/beans.xml");
		ctx.registerShutdownHook();
				
		AccountImpl a1 = ctx.getBean(AccountImpl.class);
		System.out.println(a1);
		a1.deposit(500);
		System.out.println(a1);
		
		//ctx.close();
	}
}
